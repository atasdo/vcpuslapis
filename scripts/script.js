$(document).ready(function(){
  $('.parallax').parallax();

  $('.modal').modal();

  $('.tortai-slick').slick({
   infinite: true,
   slidesToShow: 3,
   slidesToScroll: 3,
   arrows: false,
   centerMode: true,
   variableWidth: true
 });

  $('.tortai-atgal').on('click', function () {
    $('.tortai-slick').slick('slickPrev');
  })

  $('.tortai-pirmyn').on('click', function () {
    $('.tortai-slick').slick('slickNext');
  })


  $('.materialboxed').materialbox();
  

  $('.pyragai-slick').slick({
   infinite: true,
   slidesToShow: 3,
   slidesToScroll: 3,
   centerMode: true,
   variableWidth: true
 });

  $('.pyragai-atgal').on('click', function () {
    $('.pyragai-slick').slick('slickPrev');
  })

  $('.pyragai-pirmyn').on('click', function () {
    $('.pyragai-slick').slick('slickNext');
  })

  $('.sausainiai-slick').slick({
   infinite: true,
   slidesToShow: 3,
   slidesToScroll: 3,
   centerMode: true,
   variableWidth: true
 });

  $('.sausainiai-atgal').on('click', function () {
    $('.sausainiai-slick').slick('slickPrev');
  })

  $('.sausainiai-pirmyn').on('click', function () {
    $('.sausainiai-slick').slick('slickNext');
  })

});

$(document).ready(function() {
  M.updateTextFields();
});


// var prevScrollpos = window.pageYOffset;
// window.onscroll = function() {
//   var currentScrollPos = window.pageYOffset;
//   if (prevScrollpos > currentScrollPos) {
//     document.getElementById("icons_posision_hide").style.left = "0";
//   } else {
//     document.getElementById("icons_posision_hide").style.left = "-50px";
//   }
//   prevScrollpos = currentScrollPos;
// }

function myMap() {
  var mapProp= {
    center:new google.maps.LatLng(51.508742,-0.120850),
    zoom:5,
  };
  var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
