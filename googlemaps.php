    <style>
      
      #map {
        height: 550px;  
        width: 100%;  
       }
    </style>

    <div id="map"></div>
    <script>

function initMap() {
  
  var uluru = {lat: 54.679958, lng: 25.261247};

  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 17, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}
    </script>
   
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFDdDLFhPJAGuegJU5XUQ0eG6yfFrrehA&callback=initMap">
    </script>
