<div id="modal1" class="modal">
    <div class="modal-content">
      <div><h4 class="modal-heading col m12">NAPOLEONAS</h4></div>
      <div><img class="tortai-img col m6" src="images/tortai-slick1.jpg"></div>
      <div class="col m6"><p>trapūs sluoksniuotos tešlos lakštai pertepti minkštu plikytu kremu, pagardinti namine bruknių uogiene.</p></div>
      </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>

<div class="parallax-container para-lax">
      <div class="parallax"><img class="parallax-img" src="images/parallax1.jpg"></div>
  </div>

<div class="card-panel">
	<h2 class="size-fonts">Tortai</h2>
		<button class="tortai-atgal btn-flat material-icons">chevron_left</button>
		<div class="tortai-slick">
			<div><img class="tortai-img modal-trigger" data-target="modal1" src="images/tortai-slick1.jpg"></div>
		 <!--  <div><img class="tortai-img materialboxed" data-caption="NAPOLEONAS - 
		  	trapūs sluoksniuotos tešlos lakštai pertepti minkštu plikytu kremu, pagardinti namine bruknių uogiene." src="images/tortai-slick1.jpg"></div> -->
		  <div><img class="tortai-img materialboxed" data-caption="SŪRIO TORTAS - minkštas šokoladinis biskvitas su lengvu maskarponės kremu (galima pagardinti mėlynėmis, avietėmis, braškėmis, juodaisiais serbentais)." src="images/tortai-slick2.jpg"></div>
		  <div><img class="tortai-img materialboxed" data-caption="KARAMELINIS TORTAS - drėgnas ir minkštas šokoladinis biskvitas perteptas šokoladu bei karameliniu kremu" src="images/tortai-slick3.jpg"></div>
		  <div><img class="tortai-img materialboxed" data-caption="MASKARPONĖS TORTAS - purus citrininis biskvitas perteptas gaiviu maskarponės kremu, pagardintas braškėmis." src="images/tortai-slick4.jpg"></div>
		  <div><img class="tortai-img materialboxed" data-caption="SŪRIO TORTAS SU MANGU - lengvas maskarponės kremas su natūralia vanilės pasta ir mango bei pasiflorų tyrėmis ant minkšto šokoladinio biskvito." src="images/tortai-slick5.jpg"></div>
		  <div><img class="tortai-img materialboxed" data-caption="ŠOKOLADINIS AGUONŲ TORTAS - mnkštas, sodrus ir šokoladinis biskvitas su aguonomis perteptas lengvu filadelfijos sūrio kremu su natūralia vanile, tortas pagardintas džiovintomis spanguolėmis." src="images/tortai-slick6.jpg"></div>
		  <div><img class="tortai-img materialboxed" data-caption="MORKŲ TORTAS - minkštas ir prieskoniais kvepiantis morkų biskvitss perteptas lengvu filadelfijos sūrio kremu su natūralia vanile." src="images/tortai-slick7.jpg"></div>
		  <div><img class="tortai-img materialboxed" data-caption="ŠOKOLADINIS SU MASKARPONE - minkštas ir sodrus šokoladinis biskvitas perteptas lengvu maskarponės kremu, tortas pagardintas uogomis (gali būti vyšnios, braškės, avietės, juodieji serbentai)." src="images/tortai-slick8.jpg"></div>
		</div>
		<button class="tortai-pirmyn btn-flat material-icons">chevron_right</button>
</div>

<div class="parallax-container para-lax">
      <div class="parallax"><img class="parallax-img" src="images/parallax2.jpg"></div>
</div>

<div class="card-panel">
	<h2 class="size-fonts">Pyragai</h2>
		<div class="pyragai-slick">
		  <div><img class="pyragai-img" src="images/pyragai-slick1.jpg"></div>
		  <div><img class="pyragai-img" src="images/pyragai-slick2.jpg"></div>
		  <div><img class="pyragai-img" src="images/pyragai-slick3.jpg"></div>
		  <div><img class="pyragai-img" src="images/pyragai-slick2.jpg"></div>
		  <div><img class="pyragai-img" src="images/pyragai-slick3.jpg"></div>
		  <div><img class="pyragai-img" src="images/pyragai-slick2.jpg"></div>
		  <div><img class="pyragai-img" src="images/pyragai-slick1.jpg"></div>
		</div>
		<button class="pyragai-atgal btn-flat material-icons">chevron_left</button>
		<button class="pyragai-pirmyn btn-flat material-icons">chevron_right</button>
</div>
<div class="parallax-container para-lax">
      <div class="parallax"><img class="parallax-img" src="images/parallax3.jpg"></div>
</div>

<div class="card-panel">
	<h2 class="size-fonts">Sausainiai</h2>
		<div class="sausainiai-slick">
		  <div><img class="sausainiai-img" src="images/sausainiai-slick1.jpg"></div>
		  <div><img class="sausainiai-img" src="images/sausainiai-slick2.jpg"></div>
		  <div><img class="sausainiai-img" src="images/sausainiai-slick1.jpg"></div>
		  <div><img class="sausainiai-img" src="images/sausainiai-slick2.jpg"></div>
		  <div><img class="sausainiai-img" src="images/sausainiai-slick1.jpg"></div>
		  <div><img class="sausainiai-img" src="images/sausainiai-slick2.jpg"></div>
		  <div><img class="sausainiai-img" src="images/sausainiai-slick1.jpg"></div>
		  <div><img class="sausainiai-img" src="images/sausainiai-slick2.jpg"></div>
		</div>
		<button class="sausainiai-atgal btn-flat material-icons">chevron_left</button>
		<button class="sausainiai-pirmyn btn-flat material-icons">chevron_right</button>
</div>