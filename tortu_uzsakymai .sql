-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 18, 2018 at 07:53 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tortu_uzsakymai`
--

-- --------------------------------------------------------

--
-- Table structure for table `uzsakymai`
--

CREATE TABLE `uzsakymai` (
  `id` int(11) NOT NULL,
  `vardas` varchar(20) NOT NULL,
  `numeris` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `zinute` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `uzsakymai`
--

INSERT INTO `uzsakymai` (`id`, `vardas`, `numeris`, `email`, `zinute`) VALUES
(1, 'Mindaugas', '+37061497570', 'mindaugas.sharpis@gmail.com', 'Pirmas bandymas'),
(3, 'Mindaugas', '861497570', 'mindaugas@kazkas.lt', 'Antras bandymas 2'),
(4, 'Mindaugas', '861497570', 'mindaugas@kazkas.lt', 'Antras bandymas 2'),
(5, 'Kazkas', '1234567890', 'kazkas@kazkas.com', 'TreÄias bandymas'),
(6, 'Test', '123456789', 'test@test', 'Test 4'),
(7, 'Test', '123456789', 'test@test', 'Test 4'),
(8, 'Mindaugas', '+37061497570', 'kazkas@kazkas.com', 'test 5'),
(9, 'Mindaugas', '+37061497570', 'kazkas@kazkas.com', 'test6'),
(10, 'Mindaugas', '+37061497570', 'kazkas@kazkas.com', 'test6'),
(11, 'Mindaugas', '+37061497570', 'kazkas@kazkas.com', 'test6'),
(12, 'Mindaugas', '+37061497570', 'kazkas@kazkas.com', 'Test 7'),
(13, 'Mindaugas', '+37061497570', 'kazkas@kazkas.com', 'Test 7'),
(14, 'Mindaugas', '+37061497570', 'kazkas@kazkas.com', 'Test 7'),
(15, 'Mindaugas', '+37061497570', 'kazkas@kazkas.com', 'test 8'),
(16, 'Mindaugas', '+37061497570', 'kazkas@kazkas.com', 'test 8'),
(17, 'Mindaugas', '+37061497570', 'kazkas@kazkas.com', 'test 10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `uzsakymai`
--
ALTER TABLE `uzsakymai`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `uzsakymai`
--
ALTER TABLE `uzsakymai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
