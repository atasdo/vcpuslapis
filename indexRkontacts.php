<div class="row container">
  	<div class="col s6">
     <div class="center-align">
       <h4>Kotaktai</h4>
       <h5>Užsukite pas mus ar susisiekite:</h5>
     </div>
     <div class="row">
       <div class="column align-left">
        <div class="center-align">
          <ul>
            <li class="fas fa-phone" style="font-size:24px;">     8656888698</li> <br>
            <li></li><br>
            <li class="far fa-envelope" style="font-size:24px;">     sokoladaine@gmail.com</li> <br>
            <li></li><br>
            <li class="fab fa-periscope" style="font-size:24px;">     J. Basanavičiaus g. 44, Vilnius</li><br><br><br>
            <li><a class="" href="https://www.facebook.com/sokoladinesvajone/" target="_blank"> <i > <img class="icons_mine" src="images/icons/facebook.png"></a></i>
  <a class="" href="https://www.instagram.com/sokoladine_svajone/" target="_blank"> <img class="icons_mine" src="images/icons/instagram.png"></a>
  <a class="" href="https://www.pinterest.com/sokoladines/" target="_blank"> <img class="icons_mine" src="images/icons/pinterest.png"></a></li><b
          </ul>
        </div>
      </div>
    </div>			
  </div>

  <div class="col s6">
     <div class="center-align">
       <h4 >Norite gardaus deserto?</h4>
       <h5>parašykite mums</h5>
     </div>
     <div class="row">
      <form class="col s12">
        <div class="row">
          <div class="input-field col s12">
            <i class="material-icons prefix">account_circle</i>
            <input autofocus name="vardas" id="icon_prefix" type="text" class="validate" required>
            <label class="active" for="icon_prefix">Jūsų vardas</label>
          </div>
          <div class="input-field col s12">
            <i class="material-icons prefix">phone</i>
            <input name="numeris" id="icon_telephone" type="tel" class="validate" required>
            <label for="icon_telephone">Telefono numeris</label>
          </div>
          <div class="input-field col s12">
            <i class="material-icons prefix">email</i>
            <input name="email" id="icon_email" type="email" class="validate" required>
            <label for="icon_email">Elektroninis paštas</label>
          </div>  
          <div class="input-field col s12">
            <textarea name="zinute" id="textarea1" class="materialize-textarea"></textarea>
            <label class="active" for="textarea1">Jūsų žinutė</label>
          </div>
        </div>
      <button class="waves-effect waves-light btn" onClick="history.go(0)"><i class="material-icons right">send</i>Siųsti</button> 
    </form>
    </div>
  </div>
</div>


<?php include "googlemaps.php"; ?>

